import React, { useState } from 'react'
import { Grid, TextField, FormControl, Select, MenuItem, InputLabel, Button, Typography, Divider, Hidden, Snackbar } from '@material-ui/core'
import { KeyboardDatePicker } from '@material-ui/pickers'
import AccountCircle from '@material-ui/icons/AccountCircle';
import EmailIcon from '@material-ui/icons/Email';
import TodayIcon from '@material-ui/icons/Today';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import FaceIcon from '@material-ui/icons/Face';
import SendIcon from '@material-ui/icons/Send';
import Typing from 'react-typing-animation';
import useStyles from './styles'
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Registration () {
  const classes = useStyles()

  const [user, setUser] = useState({
    fullName: '',
    gender: '',
    birthdate: null,
    email: '',
    location: '',
    detailedLocation: ''
  })
  const [isDateValid, setIsDateValid] = useState(true)
  const jakartaLocations = ['Jakarta Pusat', 'Jakarta Barat', 'Jakarta Utara', 'Jakarta Selatan', 'Jakarta Timur']
  const tangerangLocations = ['Batuceper', 'Benda', 'Cibodas', 'Ciledug', 'Cipondoh', 'Jatiuwung', 'Karangtengah', 'Karawaci', 'Larangan', 'Neglasari', 'Periuk', 'Pinang', 'Tangerang']

  function handleOnChange (event) {
      const newUser = JSON.parse(JSON.stringify(user))
      newUser[event.target.name] = event.target.value
      setUser(newUser)
  }

  function handleDateOnChange (event) {
    if (event && event !== 'Invalid Date') {
      event = event.toString()
      const validationYear = new Date().getFullYear() - 21
      let dateSplit = event.split(' ')
      if (validationYear < Number(dateSplit[3])) {
        setIsDateValid(false)
      } else {
        setIsDateValid(true)
        const newUser = JSON.parse(JSON.stringify(user))
        newUser.birthdate = event
        setUser(newUser)
      }
    } else {
      const newUser = JSON.parse(JSON.stringify(user))
      newUser.birthdate = event
      setUser(newUser)
    }
  }

  function handleOnSubmit () {
    if ( user.birthdate && isDateValid) {
      const validationYear = new Date().getFullYear() - 21
      let dateSplit = user.birthdate.split(' ')
      if (validationYear < Number(dateSplit[3])) {
        setIsDateValid(false)
      } else {
        console.log('submit')
      }
    } else {
      setIsDateValid(false)
      console.log('not valid')
    }
  }

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setIsDateValid(true);
  };

  return (
    <>
      <Grid container spacing={3} justify="center" alignItems="center" style={{ height: '100vh', width: '100vw' }}>
        <Hidden smUp>
          <Grid container item xs={10} sm={6}>
            <Grid item xs={12}>
              <Typing>
                <Typography align='center' variant='h3' className={classes.titleText}>Register With <br /> E-Fair</Typography>
                <Typing.Delay ms={3000} />
                <Typing.Backspace count={30} />
                <Typography align='center' variant='h3' className={classes.titleText}>Register With <br /> E-Fair</Typography>
              </Typing>
            </Grid>
          </Grid>
        </Hidden>
        <Grid item xs={10} sm={6} md={4}>
          <form  noValidate autoComplete="off">
            <div className={classes.margin}>
              <Grid container spacing={1} alignItems="flex-end">
                <Grid item className={classes.iconControl}>
                  <AccountCircle />
                </Grid>
                <Grid item className={classes.formControl}>
                  <TextField 
                    onChange={ (event) => handleOnChange(event) } 
                    value={ user.fullName } 
                    name='fullName'
                    className={classes.form}
                    id="standard-basic"
                    label="Full Name" 
                  />
                </Grid>
              </Grid>
            </div>

            <div className={classes.margin}>
              <Grid container spacing={1} alignItems="flex-end">
                <Grid item className={classes.iconControl}>
                  <EmailIcon />
                </Grid>
                <Grid item className={classes.formControl}>
                  <TextField 
                    onChange={ (event) => handleOnChange(event) } 
                    value={ user.email } 
                    className={classes.form}
                    name='email'
                    id="standard-basic"
                    label="E-Mail" 
                  />      
                </Grid>
              </Grid>
            </div>

            <div className={classes.margin}>
              <Grid container spacing={1} alignItems="flex-end">
                <Grid item className={classes.iconControl}>
                  <FaceIcon />
                </Grid>
                <Grid item className={classes.formControl}>
                  <FormControl className={classes.form}>
                    <InputLabel>Gender</InputLabel>
                    <Select
                      name='gender'
                      value={ user.gender }
                      onChange={ (event) => handleOnChange(event) }
                    >
                      <MenuItem value={'male'}>Male</MenuItem>
                      <MenuItem value={'female'}>Female</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
              </Grid>
            </div>

            <div className={classes.margin}>
              <Grid container spacing={1} alignItems="flex-end">
                <Grid item className={classes.iconControl}>
                  <TodayIcon />
                </Grid>
                <Grid item className={classes.formControl}>
                  <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    format="MM/dd/yyyy"
                    id="date-picker-inline"
                    label="Birthdate (mm/dd/yyyy)"
                    value={ user.birthdate } 
                    onChange={ (event) => handleDateOnChange(event) } 
                    name='birthdate'
                    className={classes.form}
                    KeyboardButtonProps={{
                      'aria-label': 'change date',
                    }}
                  />
                </Grid>
              </Grid>
            </div>
            
            <div className={classes.margin}>
              <Grid container spacing={1} alignItems="flex-end">
                <Grid item className={classes.iconControl}>
                  <LocationOnIcon />
                </Grid>
                <Grid item className={classes.formControl}>
                  <FormControl className={classes.locationFormControl}>
                    <InputLabel>Location</InputLabel>
                    <Select
                      name='location'
                      value={ user.location }
                      onChange={ (event) => handleOnChange(event) }
                    >
                      <MenuItem value={'jakarta'}>Jakarta</MenuItem>
                      <MenuItem value={'tangerang'}>Tangerang</MenuItem>
                    </Select>
                  </FormControl>
                  {
                    user.location === '' ? '' : (
                      <FormControl className={classes.locationFormControl} style={{ marginLeft: '10%' }}>
                        <InputLabel>Details</InputLabel>
                        <Select
                          name='detailedLocation'
                          value={ user.detailedLocation }  
                          onChange={ (event) => handleOnChange(event) } 
                        >
                          {
                            user.location === 'jakarta' ?
                              jakartaLocations.map((location, index) => {
                                return <MenuItem value={ location } key={ index }>{ location }</MenuItem>
                              }) :
                              tangerangLocations.map((location, index) => {
                                return <MenuItem value={ location } key={ index }>{ location }</MenuItem>
                              })
                          }
                        </Select>
                      </FormControl>
                    )
                  }
                </Grid>
              </Grid>
            </div>
            
            <Button
              variant="contained"
              color="primary"
              className={classes.margin}
              onClick={ () => handleOnSubmit() }
              endIcon={<SendIcon />}
            >
              Send
            </Button>
          </form>
        </Grid>
        <Hidden xsDown>
          <Grid item sm={1} className={classes.separator} />
        </Hidden>
        <Hidden xsDown>
          <Grid container item sm={4}>
            <Grid item xs={12}>
              <Typing>
                <Typography variant='h2' className={classes.titleText}>Register With <br /> E-Fair</Typography>
                <Typing.Delay ms={3000} />
                <Typing.Backspace count={30} />
                <Typography variant='h2' className={classes.titleText}>Register With <br /> E-Fair</Typography>
              </Typing>
            </Grid>
          </Grid>
        </Hidden>
        <Snackbar open={ !isDateValid } autoHideDuration={6000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="warning">
            sorry this event is only for 21 and over
          </Alert>
        </Snackbar>
      </Grid>
    </>
  )
}