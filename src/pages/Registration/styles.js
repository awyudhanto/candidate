import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
    width: '100%'
  },
  formControl: {
    width: '90%',
  },
  form: {
    width: '100%'
  },
  iconControl: {
    width: '10%'
  },
  locationFormControl: {
    width: '45%'
  },
  titleText: {
    fontWeight: 'bolder'
  },
  separator: {
    height: '30vh',
    borderWidth: '0',
    borderRightWidth: '3px',
    BorderColor: 'black',
    borderStyle: 'solid'
  }
}));

export default useStyles