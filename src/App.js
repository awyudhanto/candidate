import './App.css';
import Registration from './pages/Registration/index'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';

function App() {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Registration />
    </MuiPickersUtilsProvider>
  );
}

export default App;
